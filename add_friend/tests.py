from django.test import TestCase

# Create your tests here.

from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest

from .models import Friend_List
from .forms import Add_Friend_Form
from .views import index, add_friend_post

# Create your tests here.

class Lab4UnitTest(TestCase):
	def test_add_friend_url_is_exist(self):
		response = Client().get('/add-friend/')
		self.assertEqual(response.status_code, 200)

	def test_using_index_func(self):
		found = resolve('/add-friend/')
		self.assertEqual(found.func, index)

	def test_name_less_than_27(self):
		friend = Friend_List(name = 'Dummy', url = 'http://google.com')
		self.assertTrue(len(friend.name) < 27)

	def test_add_success_and_render_the_result(self):
		response = Client().post('/add-friend/add_friend', {'name': 'Dummy', 'url': 'http://google.com'})
		self.assertEqual(response.status_code, 302)
		response = Client().get('/add-friend/')
		html_response = response.content.decode('utf8')
		self.assertIn('Dummy',html_response)
		self.assertIn('http://google.com',html_response)