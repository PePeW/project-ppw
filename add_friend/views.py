from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Add_Friend_Form
from .models import Friend_List



response = {} 
def index(request):
	html = 'add_friend.html'
	response['add_friend_form'] = Add_Friend_Form

	friend = Friend_List.objects.all()
	response['friend'] = friend
	return render(request, html, response)

def add_friend_post(request):
	form = Add_Friend_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		print('valid')
		response['name'] = request.POST['name']
		response['url'] = request.POST['url']
		friend = Friend_List(name=response['name'], url=response['url'])
		friend.save()

		html ='add_friend.html'

		return HttpResponseRedirect('/add-friend/')
	else:        
		return HttpResponseRedirect('/add-friend/')