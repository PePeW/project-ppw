from django import forms

class Add_Friend_Form(forms.Form):
    error_messages = {
        'required': 'Isi terlebih dahulu',

    }
    attrs_name = {
        'placeholder': 'masukkan nama',
    }
    attrs_url = {
        'placeholder': 'masukkan url',
    }

    name = forms.CharField(label='Nama', required=True, max_length=27, widget=forms.TextInput(attrs=attrs_name))
    url = forms.URLField(required=True, widget=forms.URLInput(attrs=attrs_url))

