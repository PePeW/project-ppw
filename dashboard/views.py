from django.shortcuts import render
from update_status.models import Post
from add_friend.models import Friend_List

def index(request):
	author = 'Hepzibah Smith'
	posts = Post.objects.all().count()
	
	if posts == 0:
		temp = Post(post = 'Hi')
		temp.save()
	
	latest_post = Post.objects.last().post
		
	date = Post.objects.last().created_date
	friend = Friend_List.objects.all().count()
	
	response = {'author':author, 'posts':posts, 'date':date, 'latest_post':latest_post, 'friend':friend}
	return render(request, 'statistic.html', response)
	
