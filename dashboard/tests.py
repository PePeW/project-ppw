from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
import unittest

# Create your tests here.

class DashboardTest(TestCase):
	def test_dashboard_url_is_exist(self):
		response = Client().get('/dashboard/')
		self.assertEqual(response.status_code, 200)
		
	def test_dashboard_using_index_func(self):
		found = resolve('/dashboard/')
		self.assertEqual(found.func, index)
		
	def test_statistic_template(self):
		response = Client().get('/dashboard/')
		self.assertTemplateUsed(response, 'statistic.html')