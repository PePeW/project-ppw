from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest
from datetime import date
import unittest
# Create your tests here.
class NameUnitTest(TestCase):
	def test_name_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code,301)

	def test_using_index_func(self):
		found = resolve('/')

class PageProfileUnitTest(TestCase):
	def test_page_profile_is_exist(self):
		response = Client().get('/page_profile/')
		self.assertEqual(response.status_code,200)

	def test_using_index_func(self):
		found = resolve('/page_profile/')
		self.assertEqual(found.func, index)

	def test_name_is_changed(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn(name, html_response)
		self.assertIn(bday, html_response)
		self.assertIn(gender, html_response)
		self.assertIn(expertise, html_response)
		self.assertIn(desc, html_response)
		self.assertIn(email, html_response)
		self.assertFalse(len(name) == 0)