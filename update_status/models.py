from django.db import models

class Post(models.Model):
	post = models.CharField(max_length=1600)
	created_date = models.DateTimeField(auto_now_add=True)
	
class Comment(models.Model):
	comment = models.CharField(max_length=1600)
	created_date = models.DateTimeField(auto_now_add=True)
	post = models.ForeignKey(Post, null=True, blank=True)

