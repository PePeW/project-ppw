# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-11 23:16
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('update_status', '0004_post_comments'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='comments',
            field=models.ForeignKey(default='as', on_delete=django.db.models.deletion.CASCADE, to='update_status.Comment'),
        ),
    ]
