# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-12 08:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('update_status', '0008_auto_20171012_0844'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='comments',
            field=models.ManyToManyField(to='update_status.Comment'),
        ),
    ]
