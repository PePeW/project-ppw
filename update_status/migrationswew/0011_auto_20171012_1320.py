# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-12 13:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('update_status', '0010_auto_20171012_0854'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='comments',
            field=models.ManyToManyField(blank=True, null=True, to='update_status.Comment'),
        ),
    ]
