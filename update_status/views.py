from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Post_Form
from .models import Post, Comment
from page_profile.views import response as res
# Create your views here.

response = {}
def index(request):
	html = 'update_status/updateStatus.html'
	response['post_form'] = Post_Form
	post = Post.objects.order_by('-created_date')
	com = Comment.objects.order_by('-created_date')
	response['post'] = post
	response['comments'] = com
	response['nama'] = res['name']
	return render(request, html, response)

def add_post(request):
	form = Post_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['post'] = request.POST['post']
		post = Post(post=response['post'])
		post.save()
	return HttpResponseRedirect('/update-status/')

def add_comment(request, pk):
	html = 'update_status/addComment.html'
	response['post_forms'] = Post_Form
	response['posted'] = Post.objects.get(pk=pk)
	return render(request, html, response)

def save_comment(request, pk):
	post = Post.objects.get(pk=pk)
	form = Post_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['comments'] = request.POST['post']
		com = Comment(comment=response['comments'])
		com.post = post
		com.save()
	return HttpResponseRedirect('/update-status/')
