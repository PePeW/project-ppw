from django.conf.urls import url
from .views import index, add_post, add_comment, save_comment

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^add-post', add_post, name='add-post'),
	url(r'^add-comment/(?P<pk>[0-9]+)/$', add_comment, name='add-comment'),
	url(r'^save-comment/(?P<pk>[0-9]+)/$', save_comment, name='save-comment'),
	url(r'^cancel', index, name='cancel'),
]
