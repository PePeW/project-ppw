from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.urls import resolve
from .views import index
from .models import Post
from .forms import Post_Form

# Create your tests here.
class UpdateStatusTest(TestCase):
	def test_update_status_url_is_exist(self):
		response = Client().get('/update-status/')
		self.assertEqual(response.status_code, 200)

	def test_update_status_using_index_func(self):
		found = resolve('/update-status/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_post(self):
		new_post = Post.objects.create(post="First Post")
		counting_all_available_post = Post.objects.all().count()
		self.assertEqual(counting_all_available_post,1)

	def test_form_validation_for_blank_post(self):
		form = Post_Form(data={'post': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['post'],
				["Post Tidak Boleh Kosong"]
		)

	def test_post_fail(self):
		response = Client().post('/update-status/add-post', {'post':''})
		self.assertEqual(response.status_code, 302)

	#def test_post_success_and_post_is_rendered(self):
	#	test = 'Tugas PPW'
	#	response_post = Client().post('/update-status/add-post', {'post':test})
	#	self.assertEqual(response_post.status_code, 302)
	#	response = Client().get('/update-status/')
	#	html_response = response.content.decode('utf8')
	#	self.assertIn(test, html_response)
